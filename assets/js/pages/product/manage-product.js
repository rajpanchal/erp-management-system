var TableDatatables = function(){
	var handleProductTable = function(){
		var manageProductTable = $("#manage-product-table");
		var baseURL = window.location.origin;
		var filePath = "/helper/routing.php";
		var oTable = manageProductTable.DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: baseURL + filePath,
				type: "POST",
				data: {
					"page": "manage_product"
				}
			},
			"lengthMenu": [
				[5, 15, 25, -1], //indexing
				[5, 15, 25, "All"] //to show at UI
			],
			"order": [
				[1, "desc"]
			],
			"columnDefs": [ //for which column we dont want sorting
				{
					"orderable": false,
					"targets": [0, -1]
				}
			]
		});
		
		manageProductTable.on('click','.delete',function(e){
			var id = $(this).data('id');
			$("#delete_product_id").val(id);
			$.ajax({
				url: baseURL + filePath,
				method: "POST",
				data: {
					"record_id": id,
					"fetch": "product"
				},
				dataType: "json",
				success: function(data){
					console.log(data);
					$("#edit_product_name").val(data.name);
				}
			});
		});
		new $.fn.dataTable.Buttons(oTable,{
			buttons: [
				'copy', 'csv', 'pdf'
			]
		});
		oTable.buttons().container().appendTo($('#export-buttons'));
	}
	return {
		//main function to handle all the datatables
		init: function(){
			handleProductTable();
		}
	}
}();

jQuery(document).ready(function(){
	TableDatatables.init();
});
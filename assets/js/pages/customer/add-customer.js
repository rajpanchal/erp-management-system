$(function(){
    $("#add-customer").validate({
        rules: {
            'first_name': {
                required: true
            },
            'last_name': {
                required: true
            },
            'gst_no': {
                required: true
            },
            'phone_no': {
                required: true
            },
            'email_id': {
                required: true
            },
            'block_no': {
                required: true
            },
            'street': {
                required: true
            },
            'city': {
                required: true
            },
            'pincode': {
                required: true
            },
            'state': {
                required: true
            },
            'country': {
                required: true
            },
            'town': {
                required: true
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    })
});
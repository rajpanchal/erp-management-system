var TableDatatables = function(){
	var handleSupplierTable = function(){
		var manageSupplierTable = $("#manage-supplier-table");
		var baseURL = window.location.origin;
		var filePath = "/helper/routing.php";
		var oTable=manageSupplierTable.DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: baseURL + filePath,
				type: "POST",
				data: {
					"page": "manage_supplier"
				}
			},
			"lengthMenu":[
				[5,15,25,-1], //for pagination
				[5,15,25,"All"] //-1 will be shown as All
			],
			"order":[
				[1,"desc"]
			],
			"columnDefs":[
				{
					'orderable': false,
					'targets': [0,-1]//will remove the sorting opt from 0th and last column
				}
			]
		});

		
		manageSupplierTable.on('click', '.delete', function(e){
            var id = $(this).data('id');
            // alert(id);
            $("#delete_record_id").val(id);
            $.ajax({
				url: baseURL + filePath,
				method: "POST",
				data: {
					"record_id": id,
					"fetch": "supplier"
				},
				dataType: "json",
				success: function(data){
                    // console.log("Data : " + data);
                    // console.log("Customer ID : " + data[0]["id"]);
                    $("#delete_record_id").val(data[0]["id"]);
				}
			});
		});
		
		new $.fn.dataTable.Buttons(oTable,{
			buttons:[
				'copy','csv','pdf'
			]
		});
		oTable.buttons().container().appendTo($('#export-buttons'));
	}
	return {
		//main function to handle all the datatables
		init: function(){
			handleSupplierTable();
		}
	}
}();


jQuery(document).ready(function(){
	TableDatatables.init();
});
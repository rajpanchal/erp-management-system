var id = 2;

function deleteProduct(delete_id) {
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1) {
        $("#element_" + delete_id).remove();
    }
}

function addProduct(){
    $("#products_container").append(`<!--BEGIN: PRODUCT CUSTOM CONTROL-->
    <div class="row product_row" id="element_${id}">
        <!--BEGIN: CATEGORY SELECT-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Category</label>
                <select id="category_"+${id} class="form-control">
                    <option disabled selected>Select Category</option>
                </select>
            </div>
        </div>
        <!--END: CATEGORY SELECT-->

        <!--BEGIN: PRODUCT SELECT-->
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Products</label>
                <select name="product_id[]" id="product_"+${id} class="form-control">
                    <option disabled selected>Select Product</option>
                </select>
            </div>
        </div>
        <!--END: PRODUCT SELECT-->

        <!-- BEGIN: QUANTITY -->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Quantity</label>
                <input type="text" name="quantity[]" id="quantity_"+${id} class="form-control" placeholder="Enter Quantity">
            </div>
        </div>
        <!-- END: QUANTITY -->

        <!--BEGIN: DISCOUNT-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Discount</label>
                <input type="text" name="discount[]" id="discount_"+${id} class="form-control" placeholder="Enter Discount">
            </div>
        </div>
        <!--END: DISCOUNT-->

        <!--BEGIN: SELLIBG PRICE-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Selling Price</label>
                <input type="text" name="discount[]" id="selling_price_"+${id} class="form-control" disabled>
            </div>
        </div>
        <!--END: SELLING PRICE-->

        <!--BEGIN: DELETE BUTTON-->
        <div class="col-md-1">
            <button onclick="deleteProduct(${id})" type="button" class="btn btn-danger" style="margin-top: 40%">
                <i class="far fa-trash-alt"></i>
            </button>
        </div>
        <!--END: DELETE BUTTON-->

    </div>
    <!--END: PRODUCT CUSTOM CONTROL-->`)
    id++;
}
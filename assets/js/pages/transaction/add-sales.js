var id = 2;

document.getElementsByClassName(".quantity");
function deleteProduct(delete_id) {
    var elements = document.getElementsByClassName("product_row");
    // console.log(typeof elements);
    if(elements.length != 1) {
        $("#element_" + delete_id).remove();
        calculateFinalTotal();
    }
}

function addProduct(){
    $("#products_container").append(`<!--BEGIN: PRODUCT CUSTOM CONTROL-->
    <div class="row product_row" id="element_${id}">
        <!--BEGIN: CATEGORY SELECT-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Category</label>
                <select id="category_${id}" class="form-control category_select" disabled>
                    <option disabled selected>Select Category</option>
                </select>
            </div>
        </div>
        <!--END: CATEGORY SELECT-->

        <!--BEGIN: PRODUCT SELECT-->
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Products</label>
                <select name="product_id[]" id="product_${id}" class="form-control product_select" disabled>
                    <option disabled selected>Select Product</option>
                </select>
            </div>
        </div>
        <!--END: PRODUCT SELECT-->

        <!--BEGIN: SELLIBG PRICE-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Selling Price</label>
                <input type="text" name="selling_price[]" id="selling_price_${id}" class="form-control" disabled>
            </div>
        </div>
        <!--END: SELLING PRICE-->

        <!-- BEGIN: QUANTITY -->
        <div class="col-md-1">
            <div class="form-group">
                <label for="">Quantity</label>
                <input type="number" name="quantity[]" id="quantity_${id}" class="form-control quantity" disabled value="0">
            </div>
        </div>
        <!-- END: QUANTITY -->

        <!--BEGIN: DISCOUNT-->
        <div class="col-md-1">
            <div class="form-group">
                <label for="">Discount</label>
                <input type="number" name="discount[]" id="discount_${id}" class="form-control discount" disabled value="0">
            </div>
        </div>
        <!--END: DISCOUNT-->
        
        <!--BEGIN: FINAL RATE-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Final Rate</label>
                <input type="number" name="final_rate[]" id="final_rate_${id}" class="form-control finalRate" disabled value="0">
            </div>
        </div>
        <!--END: FINAL RATE-->

        <!--BEGIN: DELETE BUTTON-->
        <div class="col-md-1">
            <button onclick="deleteProduct(${id})" type="button" class="btn btn-danger" style="margin-top: 50%">
                <i class="far fa-trash-alt"></i>
            </button>
        </div>
        <!--END: DELETE BUTTON-->

    </div>
    <!--END: PRODUCT CUSTOM CONTROL-->`)

    $("#quantity_" + id).attr("min", "0");
    $("#discount_" + id).attr("min", "0");

    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        data: {
            getCategories: true
        },
        dataType: 'json',
        success: function(categories) {
            // console.log(categories);
            categories.forEach(function (category) {
                // console.log(category);
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            id++;
        }
    });
}

$("#products_container").on("change",".category_select",function(){
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value;
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";

    $.ajax({
        url: baseURL+filePath,
        method:"POST",
        data:{
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products) {
            $("#product_"+element_id).empty();
            $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function(product){
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        }
    })
});

$("#products_container").on("change",".product_select",function(){
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";

    $.ajax({
        url: baseURL+filePath,
        method:"POST",
        data:{
            getSellingPriceByProductID: true,
            productID: product_id
        },
        dataType: 'json',
        success: function(data) {
            // console.log(data[0]);
            $('#quantity_' + element_id).prop('disabled', false);
            $('#discount_' + element_id).prop('disabled', false);
            $("#selling_price_" + element_id).val(data);
        }
    })
});

$("#products_container").on('change', '.quantity, .discount', function(){
    var element_id = $(this).attr('id').split("_")[1];
    // console.log($(this).val());
    if($(this).val() == "" || parseInt($(this).val()) < 0) {
        $(this).addClass("text-field-error");
        return;
    }
    $(this).removeClass("text-field-error");
    calculateTotal(element_id);
    calculateFinalTotal();
});

function calculateTotal(element_id) {
    var selling_price = parseInt($("#selling_price_" + element_id).val());
    var total_quantity = parseInt($("#quantity_" + element_id).val());
    var discount = parseInt($("#discount_" + element_id).val());
    var total = selling_price * total_quantity;
    if(discount >= 0) {
        total = (total_quantity * selling_price) - ((total_quantity * selling_price) * (discount / 100));
    }
    $("#final_rate_" + element_id).attr("value", total);
    if(total > 0) {
        $('#add_sales_btn').prop('disabled', false);
    }else {
        $('#add_sales_btn').prop('disabled', true);
    }
}

function calculateFinalTotal() {
    var elements = document.getElementsByClassName("finalRate");
    // console.log(elements);
    var total = 0;
    for(var i=0; i<elements.length; i++) {
        // console.log(elements[i].value);
        total += parseInt(elements[i].value);
    }
    $("#finalTotal").attr("value", total);
}

$("#check_email").click(function() {
    var email = $("#customer_email").val();
    // console.log(email);

    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";

    $.ajax({
        url: baseURL + filePath,
        method:"POST",
        data:{
            getCustomerWithEmail: true,
            email: email
        },
        dataType: 'json',
        success: function(data) {
            console.log(data);
            $("#email_verify_success").removeClass("d-inline-block");
            $("#email_verify_fail").removeClass("d-inline-block");
            $("#add_customer_btn").removeClass("d-inline-block");
            if(data.length != 0){
                $("#customer_id").attr("value", data[0].id);
                $("#email_verify_success").addClass("d-inline-block");
                $('.category_select').prop('disabled', false);
                $('.product_select').prop('disabled', false);
            }else{
                $("#customer_id").attr("value", "");
                $("#email_verify_fail").addClass("d-inline-block");
                $("#add_customer_btn").addClass("d-inline-block");
            }
        }
    })
});


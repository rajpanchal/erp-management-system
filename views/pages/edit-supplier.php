<?php
require_once __DIR__ . "/../../helper/init.php";
$page_title = "Quick ERP | Edit Supplier";
$sidebarSection = 'supplier';
$sidebarSubSection = 'manage';

// Util::dd($_GET);
$id = $_GET["id"];

// Util::dd($di->get('database')->readData("products", [], "id = $id", PDO::FETCH_OBJ));
$data = $di->get('database')->readData("suppliers", [], "id = $id", PDO::FETCH_ASSOC);

// For Address
$addressSupplier = $di->get('database')->readData("address_supplier", [], "supplier_id = $id", PDO::FETCH_ASSOC);
$addressID = $addressSupplier[0]['address_id'];
$addressDetails = $di->get('database')->readData("address", [], "id = $addressID", PDO::FETCH_ASSOC);
// Util::dd($addressDetails);

Util::createCSRFToken();
// Util::dd(Session::getSession('csrf_token'));
$errors = "";
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  // Util::dd($old);
  Session::unsetSession('old');
}

if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}

if(Session::hasSession(EDIT_SUPPLIER))
{
  Session::unsetSession(EDIT_SUPPLIER);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

<?php
    require_once __DIR__."/../includes/head-section.php";
?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php
        require_once __DIR__."/../includes/sidebar.php";
    ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__."/../includes/navbar.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 b-4 text-gray-800">Edit Supplier</h1>
            <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fas fa-list-ul fa-sm text-white"></i> Manage Supllier
            </a>
          </div>

          <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="card shadow mb-4">
                    <!-- CARD HEADER -->
                    <div class="card-header">
                      <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-plus"></i> Edit Supplier
                      </h6>
                    </div>
                    <!-- END OF CARD HEADER -->

                    <!-- CARD BODY -->
                    <div class="card-body">
                        <form action="<?= BASEURL?>helper/routing.php" method="POST" id="edit-supplier">
                          <input type="hidden" name="csrf_token" value=<?= Session::getSession('csrf_token'); ?>>
                          <input type="hidden" name="supplier_id" id="edit_supplier_id" value= <?= $data[0]["id"]; ?>> 
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('first_name') ? 'error is-invalid' : '') : ''; ?>" name="first_name" id="first_name" placeholder="Enter First Name" value="<?= $old != '' ? $old['first_name'] : $data != null ? $data[0]["first_name"]: ''; ?>"> 
                                      <?php
                                      
                                      if($errors!="" && $errors->has('first_name')):
                                          echo "<span class='error'> {$errors->first('first_name')}</span>";
                                      endif;
                                      
                                      ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('last_name') ? 'error is-invalid' : '') : ''; ?>" name="last_name" id="last_name" placeholder="Enter Last Name" value="<?= $old != '' ? $old['last_name'] : $data != null ? $data[0]["last_name"]: ''; ?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('last_name')):
                                          echo "<span class='error'> {$errors->first('last_name')}</span>";
                                      endif;
                                      
                                      ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gst_no">GST Number</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('gst_no') ? 'error is-invalid' : '') : ''; ?>" name="gst_no" id="gst_no" placeholder="Enter GST Number" value="<?= $old != '' ? $old['gst_no'] : $data != null ? $data[0]["gst_no"]: ''; ?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('gst_no')):
                                          echo "<span class='error'> {$errors->first('gst_no')}</span>";
                                      endif;
                                      
                                      ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone_no">Phone Number</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('phone_no') ? 'error is-invalid' : '') : ''; ?>" name="phone_no" id="phone_no" placeholder="Enter Phone Number" value="<?= $old != '' ? $old['phone_no'] : $data != null ? $data[0]["phone_no"]: ''; ?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('phone_no')):
                                          echo "<span class='error'> {$errors->first('phone_no')}</span>";
                                      endif;
                            
                                      ?>
                                    </div>
                                </div>
                                
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="email_id">Email ID</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('email_id') ? 'error is-invalid' : '') : ''; ?>" name="email_id" id="email_id" placeholder="Enter Email ID" value="<?= $old != '' ? $old['email_id'] : $data != null ? $data[0]["email_id"]: ''; ?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('email_id')):
                                          echo "<span class='error'> {$errors->first('email_id')}</span>";
                                      endif;
                            
                                      ?>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="company_name">Company Name</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('company_name') ? 'error is-invalid' : '') : ''; ?>" name="company_name" id="company_name" placeholder="Enter Company Name" value="<?= $old != '' ? $old['company_name'] : $data != null ? $data[0]["company_name"]: ''; ?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('email_id')):
                                          echo "<span class='error'> {$errors->first('email_id')}</span>";
                                      endif;
                            
                                      ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="block_no">Block No</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('block_no') ? 'error is-invalid' : '') : ''; ?>" name="block_no" id="block_no" placeholder="Enter Block No" value="<?= $old != '' ? $old['block_no'] : $addressDetails != null ? $addressDetails[0]["block_no"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('block_no')):
                                            echo "<span class='error'> {$errors->first('block_no')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="street">Street</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('street') ? 'error is-invalid' : '') : ''; ?>" name="street" id="street" placeholder="Enter Street" value="<?= $old != '' ? $old['street'] : $addressDetails != null ? $addressDetails[0]["street"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('street')):
                                            echo "<span class='error'> {$errors->first('street')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('city') ? 'error is-invalid' : '') : ''; ?>" name="city" id="city" placeholder="Enter City" value="<?= $old != '' ? $old['city'] : $addressDetails != null ? $addressDetails[0]["city"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('city')):
                                            echo "<span class='error'> {$errors->first('city')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="pincode">Pincode</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('pincode') ? 'error is-invalid' : '') : ''; ?>" name="pincode" id="pincode" placeholder="Enter Pincode" value="<?= $old != '' ? $old['pincode'] : $addressDetails != null ? $addressDetails[0]["pincode"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('pincode')):
                                            echo "<span class='error'> {$errors->first('pincode')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('state') ? 'error is-invalid' : '') : ''; ?>" name="state" id="state" placeholder="Enter State" value="<?= $old != '' ? $old['state'] : $addressDetails != null ? $addressDetails[0]["state"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('state')):
                                            echo "<span class='error'> {$errors->first('state')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('country') ? 'error is-invalid' : '') : ''; ?>" name="country" id="country" placeholder="Enter Country" value="<?= $old != '' ? $old['country'] : $addressDetails != null ? $addressDetails[0]["country"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('country')):
                                            echo "<span class='error'> {$errors->first('country')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="town">Town</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('town') ? 'error is-invalid' : '') : ''; ?>" name="town" id="town" placeholder="Enter Town" value="<?= $old != '' ? $old['town'] : $addressDetails != null ? $addressDetails[0]["town"]: ''; ?>">
                                        <?php
                                        
                                        if($errors!="" && $errors->has('town')):
                                            echo "<span class='error'> {$errors->first('town')}</span>";
                                        endif;
                                        
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <input type="submit" class="btn btn-primary" name="editSupplier" value="Submit" id="editSupplier">
                        </form>
                    </div>
                    <!-- END OF CARD BODY -->    
                    
                  </div>
                </div>
              </div>
          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php
                require_once __DIR__."/../includes/footer.php";
          ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?> 

  <!-- Logout Modal-->
  
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?> 

  <script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
  <!-- <script src="<?=BASEASSETS;?>js/pages/customer/manage-customer.js"></script> -->
  
</body>

</html>

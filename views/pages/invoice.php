<?php
require_once __DIR__ . "/../../helper/init.php";
$page_title = "Quick ERP | Blank Template";

if(isset($_GET))
{
  // Util::dd($_GET["id"]);
  $invoice_id = $_GET["id"];
  // $customer = $di->get('database')->readData("invoice", ['customer_id'], "id = ${invoice_id} AND deleted = 0", PDO::FETCH_ASSOC);
  // $customer_id = $customer[0]["customer_id"];
  $query = "SELECT t1.customer_id, t2.first_name, t2. FROM invoice t1 INNER JOIN customer t2 WHERE id = {$invoice_id} AND deleted = 0";
  $customer = $di->get('database')->raw($query, PDO::FETCH_ASSOC);
  // Util::dd($di->get('database')->raw($cust, PDO::FETCH_ASSOC));
  // Util::dd($customer[0]["customer_id"]);
  $customer_id = $customer[0]["customer_id"];
  

  // $customer_details = $di->get('database')->readData("customer", [], "id = ${customer_id} AND deleted = 0", PDO::FETCH_ASSOC);

  $query = "SELECT * FROM customer WHERE id = {$customer_id} AND deleted = 0";
  $customer_details = $di->get('database')->raw($query, PDO::FETCH_ASSOC);
  // Util::dd($customer_details);
  print_r($customer_details);

  $query = "SELECT * FROM sales WHERE invoice_id = {$invoice_id} AND deleted = 0";
  $sales_details = $di->get('database')->raw($query, PDO::FETCH_ASSOC);
  print_r($sales_details);

  // $selling_rate = "SELECT t1.product_id, t1.selling_rate, t1.with_effect_from FROM products_selling_rate t1 INNER JOIN (SELECT products_selling_rate.product_id, selling_rate, MAX(with_effect_from) AS wef FROM products_selling_rate, sales WHERE with_effect_from <= sales.created_at GROUP BY products_selling_rate.product_id = {$product_id}) t2 ON t2.wef = t1.with_effect_from AND t1.product_id = {$product_id}";


}

?>

<!DOCTYPE html>
<html lang="en">

<head>

<?php
    require_once __DIR__."/../includes/head-section.php";
?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php  require_once __DIR__ ."/../includes/navbar.php";?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Invoice</h1>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php
                require_once __DIR__."/../includes/footer.php";
          ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?> 

  <!-- Logout Modal-->
  

  <?php require_once __DIR__."/../includes/core-scripts.php"; ?> 

</body>

</html>

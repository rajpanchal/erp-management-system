<?php

define('BASEURL', $di->get('config')->get('base_url'));
define('BASEASSETS', BASEURL."assets/");
define('BASEPAGES', BASEURL."views/pages/");

define("ADD_SUCCESS", "Add sucess");
define("ADD_ERROR", "Add error");
define("UPDATE_SUCCESS", "Update sucess");
define("UPDATE_ERROR", "Update error");
define("DELETE_SUCCESS", "Delete sucess");
define("DELETE_ERROR", "Delete error");
define("VALIDATION_ERROR", "validation error");
define("EDIT_CUSTOMER", "Editing Customer");
define("EDIT_SUPPLIER", "Editing Customer");
define("EDIT_PRODUCT", "Editing Customer");
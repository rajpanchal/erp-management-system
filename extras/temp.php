<?php
/** 
 * add-customer.php
 */
?>

<div class="col-md-3">
    <div class="form-group">
        <label for="block_no">Block No</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('block_no') ? 'error is-invalid' : '') : ''; ?>" name="block_no" id="block_no" placeholder="Enter Block No" value="<?= $old != '' ? $old['block_no']: '';?>">
<?php
        if($errors!="" && $errors->has('block_no')):
        echo "<span class='error'> {$errors->first('block_no')}</span>";
        endif;
?>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="street">Street</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('street') ? 'error is-invalid' : '') : ''; ?>" name="street" id="street" placeholder="Enter Street" value="<?= $old != '' ? $old['street']: '';?>">
<?php
        if($errors!="" && $errors->has('street')):
        echo "<span class='error'> {$errors->first('street')}</span>";
        endif;
?>
        </div>
    </div>
<div class="col-md-5">
    <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('city') ? 'error is-invalid' : '') : ''; ?>" name="city" id="city" placeholder="Enter City" value="<?= $old != '' ? $old['city']: '';?>">
<?php
        if($errors!="" && $errors->has('city')):
        echo "<span class='error'> {$errors->first('city')}</span>";
        endif;
?>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <label for="pincode">Pincode</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('pincode') ? 'error is-invalid' : '') : ''; ?>" name="pincode" id="pincode" placeholder="Enter Pincode" value="<?= $old != '' ? $old['pincode']: '';?>">
<?php
        if($errors!="" && $errors->has('pincode')):
        echo "<span class='error'> {$errors->first('pincode')}</span>";
        endif;
?>
    </div>
</div>

<div class="col-md-5">
    <div class="form-group">
        <label for="state">State</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('state') ? 'error is-invalid' : '') : ''; ?>" name="state" id="state" placeholder="Enter State" value="<?= $old != '' ? $old['state']: '';?>">
<?php
        if($errors!="" && $errors->has('state')):
        echo "<span class='error'> {$errors->first('state')}</span>";
        endif;
?>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="country">Country</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('country') ? 'error is-invalid' : '') : ''; ?>" name="country" id="country" placeholder="Enter Country" value="<?= $old != '' ? $old['country']: '';?>">
<?php
        if($errors!="" && $errors->has('country')):
        echo "<span class='error'> {$errors->first('country')}</span>";
        endif;
?>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="town">Town</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('town') ? 'error is-invalid' : '') : ''; ?>" name="town" id="town" placeholder="Enter Town" value="<?= $old != '' ? $old['town']: '';?>">
<?php
        if($errors!="" && $errors->has('town')):
        echo "<span class='error'> {$errors->first('town')}</span>";
        endif;
?>
    </div>
</div>



<?php
/** 
 * edit-customer.php
 */
?>

<div class="col-md-3">
    <div class="form-group">
        <label for="block_no">Block No</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('block_no') ? 'error is-invalid' : '') : ''; ?>" name="block_no" id="block_no" placeholder="Enter Block No" value="<?= $old != '' ? $old['block_no'] : $addressDetails != null ? $addressDetails[0]["block_no"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('block_no')):
            echo "<span class='error'> {$errors->first('block_no')}</span>";
        endif;
        
        ?>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label for="street">Street</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('street') ? 'error is-invalid' : '') : ''; ?>" name="street" id="street" placeholder="Enter Street" value="<?= $old != '' ? $old['street'] : $addressDetails != null ? $addressDetails[0]["street"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('street')):
            echo "<span class='error'> {$errors->first('street')}</span>";
        endif;
        
        ?>
    </div>
</div>
<div class="col-md-5">
    <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('city') ? 'error is-invalid' : '') : ''; ?>" name="city" id="city" placeholder="Enter City" value="<?= $old != '' ? $old['city'] : $addressDetails != null ? $addressDetails[0]["city"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('city')):
            echo "<span class='error'> {$errors->first('city')}</span>";
        endif;
        
        ?>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="pincode">Pincode</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('pincode') ? 'error is-invalid' : '') : ''; ?>" name="pincode" id="pincode" placeholder="Enter Pincode" value="<?= $old != '' ? $old['pincode'] : $addressDetails != null ? $addressDetails[0]["pincode"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('pincode')):
            echo "<span class='error'> {$errors->first('pincode')}</span>";
        endif;
        
        ?>
    </div>
</div>
<div class="col-md-5">
    <div class="form-group">
        <label for="state">State</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('state') ? 'error is-invalid' : '') : ''; ?>" name="state" id="state" placeholder="Enter State" value="<?= $old != '' ? $old['state'] : $addressDetails != null ? $addressDetails[0]["state"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('state')):
            echo "<span class='error'> {$errors->first('state')}</span>";
        endif;
        
        ?>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label for="country">Country</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('country') ? 'error is-invalid' : '') : ''; ?>" name="country" id="country" placeholder="Enter Country" value="<?= $old != '' ? $old['country'] : $addressDetails != null ? $addressDetails[0]["country"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('country')):
            echo "<span class='error'> {$errors->first('country')}</span>";
        endif;
        
        ?>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label for="town">Town</label>
        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('town') ? 'error is-invalid' : '') : ''; ?>" name="town" id="town" placeholder="Enter Town" value="<?= $old != '' ? $old['town'] : $addressDetails != null ? $addressDetails[0]["town"]: ''; ?>">
        <?php
        
        if($errors!="" && $errors->has('town')):
            echo "<span class='error'> {$errors->first('town')}</span>";
        endif;
        
        ?>
    </div>
</div>
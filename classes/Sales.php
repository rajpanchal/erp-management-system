<?php

class Sales
{
    private $table = "sales";
    private $columns = ['id', 'product_id', 'quantity', 'discount', 'invoice_id', 'created_at', 'updated_at', 'deleted'];
    protected $di;
    private $database;
    private $validator;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }

    public function validateData($data, $id = 0)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data, [
            'product_id' => [
                'required'=>true,
            ],
            'quantity' => [
                'required'=>true,
            ],
            'invoice_id' => [
                'required'=>true,
            ]
        ]);
    }

    public function getValidator()
    {
            return $this->validator;
    }
    public function create($data)
    {   
        // Util::dd($data["discount"][0]);
        //VALIDATE DATA
        // $this->validateData($data);

        // //INSERT DATA IN DATABASE
        // if(!$this->validator->fails())
        // {
            // Util:dd($data);
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'customer_id'=>$data['customer_id']
                ];
                $invoice_id = $this->database->insert('invoice', $data_to_be_inserted);
                // Util::dd($invoice_id);
                // Adding Products in sales table
                $data_for_sales = [];
                $data_for_sales['invoice_id'] = $invoice_id;
                // Util::dd($data["discount"][0]);
                for($i=0; $i<count($data["product_id"]); $i++){
                    $data_for_sales['product_id'] = $data["product_id"][$i];
                    $data_for_sales['quantity'] = $data["quantity"][$i];
                    $data_for_sales['discount'] = $data["discount"][$i];
                    $this->database->insert('sales', $data_for_sales);
                    // Util::dd($this->database->insert('sales', $data_for_sales));
                }
                $this->database->commit();
                // Util::redirect("invoice.php?id=".$invoice_id);
                return $invoice_id;
            }catch(Exception $e){
                $this->database->rollBack();
                // Util::dd($e);
                return ADD_ERROR;
            }
        }
    //     return VALIDATION_ERROR;
    // }
    
}
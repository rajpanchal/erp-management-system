<?php

class Customer
{
    private $table = "customer";
    private $columns = ['id', 'first_name', 'last_name', 'gst_no', 'phone_no', 'email_id', 'gender', 'created_at', 'updated_at'];
    protected $di;
    private $database;
    private $validator;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }

    public function validateData($data, $id = 0)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 3,
                'maxlength' => 20,
            ],
            'last_name' => [
                'required' => true,
                'minlength' => 3,
                'maxlength' => 20,
            ],
            'gst_no' => [
                'required' => true,
                'length' => 15,
                'unique' => "{$this->table}, {$id}",
            ],
            'phone_no' => [
                'required' => true,
                'length' => 10,
                'unique' => "{$this->table}, {$id}",
            ],
            'email_id' => [
                'required' => true,
                'maxlength' => 255,
                'email' => true,
                'unique' => "{$this->table}, {$id}",
            ],
            'block_no' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 30,
            ],
            'street' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 25,
            ],
            'city' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 25,
            ],
            'pincode' => [
                'required' => true,
                'length' => 6,
            ],
            'state' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 25,
            ],
            'country' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 25,
            ],
            'town' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 25,
            ],
        ]);
    }

    public function getValidator()
    {
            return $this->validator;
    }
    public function addCustomer($data)
    {
        //VALIDATE DATA
        $this->validateData($data);

        //INSERT DATA IN DATABASE
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'first_name'=>$data['first_name'], 
                    'last_name'=>$data['last_name'],
                    'gst_no'=>$data['gst_no'],
                    'phone_no'=>$data['phone_no'],
                    'email_id'=>$data['email_id'],
                    'gender'=>$data['gender']
                ];
                $customer_id = $this->database->insert($this->table, $data_to_be_inserted);

                // For Adding Address 
                $data_to_be_inserted = [
                    'block_no'=>$data['block_no'],
                    'street'=>$data['street'],
                    'city'=>$data['city'],
                    'pincode'=>$data['pincode'],
                    'state'=>$data['state'],
                    'country'=>$data['country'],
                    'town'=>$data['town']
                ];
                $address_id = $this->database->insert('address', $data_to_be_inserted);

                $data_to_be_inserted = [
                    'address_id'=> $address_id,
                    'customer_id'=> $customer_id
                ];
                $address_customer_id = $this->database->insert('address_customer', $data_to_be_inserted);

                $this->database->commit();
                return ADD_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }
    public function getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length)
    {
        // $query = "SELECT * FROM {$this->table} WHERE deleted = 0";

        // $query2 = "SELECT address_id FROM address_customer, customer WHERE address_customer.customer_id = customer.id";

        // $querySmall = "SELECT address_id FROM address_customer INNER JOIN customer ON address_customer.customer_id = customer.id";

        // SELECT CONCAT(block_no, \", \", street, \", \", city, \" - \", pincode, \".\" , \" \", state, \".\", \" \", country, \".\", \" \", town) as full_address FROM address WHERE id IN (SELECT address_id FROM address_customer, customer WHERE address_customer.customer_id = customer.id) AND deleted = 0
        // SELECT CONCAT(block_no, ", ", street, ", ", city, " - ", pincode, "." , " ", state, ".", " ", country, ".", " ", town) as full_address FROM address WHERE id IN (SELECT address_id FROM address_customer, customer WHERE address_customer.customer_id = customer.id) AND deleted = 0

        // $tempQuery = "SELECT address_customer.customer_id, CONCAT(block_no, \", \", street, \", \", city, \" - \", pincode, \".\" , \" \", state, \".\", \" \", country, \".\", \" \", town) as full_address FROM address INNER JOIN address_customer ON address.id = (SELECT address_id FROM address_customer INNER JOIN customer ON address_customer.customer_id = customer.id) AND deleted = 0";

        // $queryy = "SELECT address_customer.customer_id, CONCAT(block_no, ", ", street, ", ", city, " - ", pincode, "." , " ", state, ".", " ", country, ".", " ", town) as full_address FROM address INNER JOIN address_customer WHERE address.id = address_customer.address_id AND deleted = 0";

        $query = "SELECT customer.id, customer.first_name, customer.last_name, customer.gst_no, customer.phone_no, customer.email_id, cust_addr.full_address, customer.gender, customer.created_at, customer.updated_at FROM customer INNER JOIN (SELECT address_customer.customer_id, CONCAT(block_no, \", \", street, \", \", city, \" - \", pincode, \".\" , \" \", state, \".\", \" \", country, \".\", \" \", town) as full_address FROM address INNER JOIN address_customer WHERE address.id = address_customer.address_id AND deleted = 0) as cust_addr ON cust_addr.customer_id = customer.id";

        // Util::dd($query);

        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";

        if($search_parameter != null)
        {
            $query .= " AND first_name LIKE '%{$search_parameter}%' 
            OR last_name LIKE '%{$search_parameter}%'
            OR gst_no LIKE '%{$search_parameter}%'
            OR phone_no LIKE '%{$search_parameter}%'
            OR email_id LIKE '%{$search_parameter}%'
            OR gender LIKE '%{$search_parameter}%'
            OR created_at LIKE '%{$search_parameter}%'
            OR updated_at LIKE '%{$search_parameter}%'
            ";
            $filteredRowCountQuery .= " AND first_name LIKE '%{$search_parameter}%' 
            OR last_name LIKE '%{$search_parameter}%'
            OR gst_no LIKE '%{$search_parameter}%'
            OR phone_no LIKE '%{$search_parameter}%'
            OR email_id LIKE '%{$search_parameter}%'
            OR gender LIKE '%{$search_parameter}%'
            OR created_at LIKE '%{$search_parameter}%'
            OR updated_at LIKE '%{$search_parameter}%'
            "; // " AND (name LIKE '%{$search_parameter}%' OR )";
        }

        // Util::dd($this->columns[$order_by[0]['column']]);

        if($order_by != null)
        {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        }
        else{
            $query .= " ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[0]} ASC";
        }

        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
        }
        // Util::dd($query);

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;

        $fetchedData = $this->database->raw($query);
        $data = [];
        $basePages = BASEPAGES;
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        for($i=0; $i<$numRows; $i++)
        {
            $subArray = [];
            $subArray[] = $start+$i+1;
            $subArray[] = $fetchedData[$i]->first_name;
            $subArray[] = $fetchedData[$i]->last_name;
            $subArray[] = $fetchedData[$i]->gst_no;
            $subArray[] = $fetchedData[$i]->phone_no;
            $subArray[] = $fetchedData[$i]->email_id;
            $subArray[] = $fetchedData[$i]->full_address;
            $subArray[] = $fetchedData[$i]->gender;
            $subArray[] = $fetchedData[$i]->created_at;
            $subArray[] = $fetchedData[$i]->updated_at;
            //here doc
            $subArray[] = <<<BUTTONS
<a href="{$basePages}edit-customer.php?id={$fetchedData[$i]->id}" class='btn btn-outline-primary btn-sm'>
<i class="fas fa-pencil-alt"></i></a>
<button class='btn btn-outline-danger btn-sm delete' data-id='{$fetchedData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash-alt"></i></button>
BUTTONS;

            $data[] = $subArray;
        }

        $output = array(
            'draw'=>$draw,
            'recordsTotal'=>$numberOfTotalRows,
            'recordsFiltered'=>$numberOfFilteredRows,
            'data'=>$data
        );
        echo json_encode($output);
    }

    public function getCustomerByID($id, $fetchMode = PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table} WHERE id = {$id} AND deleted = 0";
        $result = $this->database->raw($query, $fetchMode);
        return $result;
    }
    public function getCustomerWithEmail($email, $fetchMode = PDO::FETCH_OBJ) 
    {
        $query = "SELECT * FROM {$this->table} WHERE email_id = '{$email}' AND deleted = 0";
        // Util::dd($query);
        $result = $this->database->raw($query, $fetchMode);
        return $result;
    }

    public function getAddressByCustomerID($id, $fetchMode = PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM `address_customer` WHERE customer_id = {$id}";
        // Util::dd($query);
        $result = $this->database->raw($query, $fetchMode);
        // Util::dd($result);

        // For Multiple Address
        // if(count($result) > 1)
        // {
        //     foreach($id as $result)
        //     {
        //         $query = "SELECT * FROM `address` WHERE id = {$id[0]['address_id']} AND deleted = 0";
        //         $allAddress = $this->database->raw($query, $fetchMode);
        //         // Util::dd($allAddress);
        //         return $allAddress;
        //     }
        // }

        $query = "SELECT * FROM `address` WHERE id = {$result[0]['address_id']} AND deleted = 0";
        $result = $this->database->raw($query, $fetchMode);
        return $result;
    }

    public function update($data, $id)
    {
        $customer_table_to_be_updated = [
            'first_name'=>$data['first_name'], 
            'last_name'=>$data['last_name'],
            'gst_no'=>$data['gst_no'],
            'phone_no'=>$data['phone_no'],
            'email_id'=>$data['email_id'],
            'gender'=>$data['gender']
        ];
        $this->validateData($customer_table_to_be_updated, $id);

        // For Updating Address
        $address_table_to_be_inserted = [
            'block_no'=>$data['block_no'],
            'street'=>$data['street'],
            'city'=>$data['city'],
            'pincode'=>$data['pincode'],
            'state'=>$data['state'],
            'country'=>$data['country'],
            'town'=>$data['town']
        ];
  
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $query = "SELECT * FROM `address_customer` WHERE customer_id = {$id}";
                $addressID = $this->database->raw($query, PDO::FETCH_ASSOC);
                // Util::dd($addressID[0]["address_id"]);
                // Util::dd($data_to_be_updated);
                $result = $this->database->update($this->table, $customer_table_to_be_updated, "id = {$id}" );
                $address_id = $this->database->update('address', $address_table_to_be_inserted, "id = {$addressID[0]['address_id']}" );
                // Util::dd($result);
                $this->database->commit();
                return UPDATE_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }
    public function delete($id)
    {
        // Util::dd($id);
        try{
            $this->database->beginTransaction();
            $query = "SELECT * FROM `address_customer` WHERE customer_id = {$id}";
            $addressID = $this->database->raw($query, PDO::FETCH_ASSOC);
            $this->database->delete($this->table, "id = {$id}");
            $this->database->delete('address', "id = {$addressID[0]['address_id']}");
            $this->database->commit();
            return DELETE_SUCCESS;
        }catch(Exception $e){
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }
}
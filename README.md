# ERP Management System
ERP Management System based on PHP with GST

## Features
1. Manages customer, suppliers and products.
2. Automates GST calculation.
    > GST - Goods and Services Tax is an indirect tax used in India on the supply of goods and services.

## Upcoming Feature
1. Creates printable invoice

## Build With
1. HTML
2. CSS
3. Javascript
4. JQuery v3.4.1
5. PHP <= v7.2.31

## Plugins Used
1. [Bootstrap v4.3.1](https://getbootstrap.com/)
2. [jQuery Validation Plugin v1.14.0](http://jqueryvalidation.org/)
3. [toastr v2.1.3](https://cdnjs.com/libraries/toastr.js)
4. [DataTables](https://datatables.net/)
    **<p> Included libraries</p>**
    
    | Library | Version |
    | ------- | ------- |
    | DataTables | 1.10.20 |
    | Buttons | 1.6.1 |
    | pdfmake | 0.1.36 |
    | Flash export | 1.6.1|
    | HTML5 export | 1.6.1 |
    | SearchPanes | 1.0.1 |
    | Scroller | 2.0.1 |
    | Select | 1.3.1 |
5. [Chart.js v2.8.0](https://www.chartjs.org) 

## Dashboard Template Used
Start Bootstrap - [SB Admin 2 v4.0.7](https://startbootstrap.com/template-overviews/sb-admin-2)

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)

